![](https://elioway.gitlab.io/eliosin/toothpaste/elio-toothpaste-logo.png)

> What goes around, comes around **the elioWay**

# toothpaste ![notstarted](https://elioway.gitlab.io/eliosin/icon/devops/notstarted/favicon.ico "notstarted")

**toothpaste** creates a rule which will puts listed tags alternatively into heaven, then hell, then heaven, then hell..

- [toothpaste Documentation](https://elioway.gitlab.io/eliosin/toothpaste)

## Installing

```shell
npm install @elioway/toothpaste --save
yarn add  @elioway/toothpaste --dev
```

- [Installing toothpaste](https://elioway.gitlab.io/eliosin/toothpaste/installing.html)

## Nutshell

### `gulp`

### `npm run test`

### `npm run prettier`

- [toothpaste Credits](https://elioway.gitlab.io/eliosin/toothpaste/credits.html)

![](https://elioway.gitlab.io/eliosin/toothpaste/apple-touch-icon.png)

## License

[HTML5 Boilerplate](LICENSE.txt) [Tim Bushell](mailto:theElioWay@gmail.com)
