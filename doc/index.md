<aside>
  <dl>
  <dd>for what peace will be given</dd>
  <dd>To us enslaved, but custody severe,</dd>
  <dd>And stripes</dd>
</dl>
</aside>

**toothpaste** extends **god** with an apocalyptic and simple rule. tagNames in heaven, and tagNames in hell switch alteratively from hell to heaven.

# Nutshell

`toothpaste` - in principle - works in elioWays:

- When a tagName is in _hell_, the first appears in _hell_
- ... the next appears in _heaven_
- ... the next appears in _hell_
- ... the next appears in _heaven_
- ... the next appears in _hell_
- ... the next appears in _heaven_
- ... the next appears in _hell_

# toothpaste's only commandment is:

1. **toothpaste** only works in heaven and hell

_Terms and conditions apply. The **god** retains the right to change these rules at a whim._
